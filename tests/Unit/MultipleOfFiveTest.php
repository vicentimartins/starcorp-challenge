<?php

namespace Test\Unit;

use App\MultipleOfFive;
use PHPUnit\Framework\TestCase;

class MultipleOfFiveTest extends TestCase
{
    public function testWordExistsForMultipleOfFive()
    {
        $this->assertSame('IT', MultipleOfFive::WORD);
    }

    /**
     * @dataProvider Test\Providers\MultiplesProvider::multiplesOfFiveProvider
     */
    public function testConvertMultiplesReturnsRuleForMultiplesOfFive($data): void
    {
        $instance = new MultipleOfFive();
        $this->assertEquals(
            $data,
            $instance->convertMultiples(range(1, 100), [5, 5], MultipleOfFive::WORD)
        );
    }
}
