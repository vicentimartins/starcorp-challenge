# Challenge - Backend Developer

Using SOLID principles, write a program that prints all the numbers from 1 to 100. However, for multiples of 3, instead of the number, print “StarCorp”. For multiples of 5 print "IT". For numbers which are multiples of both 3 and 5, print “StarCorpianos”.

But here's the catch: you can use only one `if`. No multiple branches, ternary operators or `else`.

## Requirements

* 1 if

* You can't use `else`, `else if` or ternary

* Unit tests

* You can write the challenge in any language you want. Here at StarCorp we are big fans of PHP, Kotlin, .Net C# and TypeScript

## Submission

You can create a public repository on your GitHub account and send the link to us.

## How to run the challenge

*This "how to" assume that you have **Docker** and **Docker Compose** installed at your localhost.*

* Clone this project for your localhost

    `git clone git@gitlab.com:vicentimartins/starcorp-challenge.git`

    or

    `git clone https://gitlab.com/vicentimartins/starcorp-challenge.git`

* Go to path that was created by git clone

    `cd starcorp-challenge`

* Run docker-compose

    `docker-compose up -d --build`

* Install challenge dependencies

    `docker-compose exec php bash`

    This command will direct you to inside of container created. Now, you need to run composer:

    `composer install -o`

* Run the challenge

    Inside the php container yet, call `index.php` file like this:

    `php index.php`

* To run challenge's tests, follow:

    `composer test`

    This command use composer script, but in real it use `vendor/bin/phpunit --testdox ./test` to run it tests.
