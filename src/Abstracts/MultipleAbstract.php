<?php

namespace App\Abstracts;

use App\Contracts\MultipleInterface;

abstract class MultipleAbstract implements MultipleInterface
{
    public function convertMultiples(array $multiples, array $factor, string $word): array
    {
        $result = [];
        foreach ($multiples as $item) {
            if (0 === $item % $factor[0] && 0 === $item % $factor[1]) {
                $result[$item] = $word;
            }
        }
        return $result;
    }
}