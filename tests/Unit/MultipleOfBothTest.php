<?php

namespace Test\Unit;

use App\MultipleOfBoth;
use PHPUnit\Framework\TestCase;

class MultipleOfBothTest extends TestCase
{
    public function testWordExistsForMultipleOfBoth(): void
    {
        $this->assertSame('StarCorpianos', MultipleOfBoth::WORD);
    }

    /**
     * @dataProvider Test\Providers\MultiplesProvider::multiplesOfBothProvider
     */
    public function testConvertMultiplesReturnsRuleForMultiplesOfBoth($data): void
    {
        $instance = new MultipleOfBoth();
        $this->assertEquals(
            $data,
            $instance->convertMultiples(range(1, 100), [3, 5], MultipleOfBoth::WORD)
        );
    }
}
