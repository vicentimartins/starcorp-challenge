<?php

namespace App\Contracts;

interface StarCorpInterface
{
    public function run(): void;
}
