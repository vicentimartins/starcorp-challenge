<?php

namespace App;

use App\MultipleOfBoth;
use App\MultipleOfThree;
use App\Contracts\StarCorpInterface;

class StarCorp implements StarCorpInterface
{
    public function generateMultiplesOfThree(): array
    {
        $instance = new MultipleOfThree();
        $factor = [3, 3];
        return $instance->convertMultiples(
            range(1,100),
            $factor,
            MultipleOfThree::WORD
        );
    }

    public function generateMultiplesOfFive(): array
    {
        $instance = new MultipleOfFive();
        $factor = [5, 5];
        return $instance->convertMultiples(
            range(1, 100),
            $factor,
            MultipleOfFive::WORD
        );
    }

    public function generateMultiplesOfBoth(): array
    {
        $instance = new MultipleOfBoth();
        $factor = [3, 5];
        return $instance->convertMultiples(
            range(1, 100),
            $factor,
            MultipleOfBoth::WORD
        );
    }

    public function mergeMultiples(): array
    {
        $multipleOfBoth = $this->generateMultiplesOfBoth();
        $multipleOfFive = $this->generateMultiplesOfFive();
        $multipleOfThree = $this->generateMultiplesOfThree();
        $multiples = array_combine(range(1, 100), range(1, 100));
        foreach ($multipleOfBoth as $key => $valeu) {
            $multipleOfFive[$key] = $valeu;
        }
        
        foreach ($multipleOfFive as $key => $value) {
            $multipleOfThree[$key] = $value;
        }

        ksort($multipleOfThree);
        foreach ($multipleOfThree as $key => $value) {
            $multiples[$key] = $value;
        }

        return $multiples;
    }

    public function convertArrayToString(array $multiples): string
    {
        $result = '';
        foreach ($multiples as $item) {
            $result .= "{$item} ";
        }
        return $result . PHP_EOL;
    }

    public function run(): void
    {
        echo $this->convertArrayToString($this->mergeMultiples());
    }
}
