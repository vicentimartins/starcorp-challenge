<?php

namespace Test\Unit;

use PHPUnit\Framework\TestCase;
use App\StarCorp;

class StarCorpTest extends TestCase
{
    public function testConvertArrayToStringRunsOk(): void
    {
        $instance = new StarCorp();
        $this->assertEquals(
            'Tests runs well ' . PHP_EOL,
            $instance->convertArrayToString(['Tests', 'runs', 'well'])
        );
    }

    /**
     * @dataProvider Test\Providers\MultiplesProvider::mergeMultiplesProvider
     */
    public function testMergeMultiplesRunsOk($data): void
    {
        $instance = new StarCorp();
        $this->assertEquals(
            $data,
            $instance->mergeMultiples()
        );
    }
}
