<?php

namespace Test\Providers;

class MultiplesProvider
{
    public function multiplesOfThreeProvider(): array
    {
        return [
            [
                [
                    3 => 'StarCorp',
                    6 => 'StarCorp',
                    9 => 'StarCorp',
                    12 => 'StarCorp',
                    15 => 'StarCorp',
                    18 => 'StarCorp',
                    21 => 'StarCorp',
                    24 => 'StarCorp',
                    27 => 'StarCorp',
                    30 => 'StarCorp',
                    33 => 'StarCorp',
                    36 => 'StarCorp',
                    39 => 'StarCorp',
                    42 => 'StarCorp',
                    45 => 'StarCorp',
                    48 => 'StarCorp',
                    51 => 'StarCorp',
                    54 => 'StarCorp',
                    57 => 'StarCorp',
                    60 => 'StarCorp',
                    63 => 'StarCorp',
                    66 => 'StarCorp',
                    69 => 'StarCorp',
                    72 => 'StarCorp',
                    75 => 'StarCorp',
                    78 => 'StarCorp',
                    81 => 'StarCorp',
                    84 => 'StarCorp',
                    87 => 'StarCorp',
                    90 => 'StarCorp',
                    93 => 'StarCorp',
                    96 => 'StarCorp',
                    99 => 'StarCorp',
                ]
            ]
        ];
    }

    public function multiplesOfFiveProvider(): array
    {
        return [
            [
                [
                    5 => 'IT',
                    10 => 'IT',
                    15 => 'IT',
                    20 => 'IT',
                    25 => 'IT',
                    30 => 'IT',
                    35 => 'IT',
                    40 => 'IT',
                    45 => 'IT',
                    50 => 'IT',
                    55 => 'IT',
                    60 => 'IT',
                    65 => 'IT',
                    70 => 'IT',
                    75 => 'IT',
                    80 => 'IT',
                    85 => 'IT',
                    90 => 'IT',
                    95 => 'IT',
                    100 => 'IT',
                ]
            ]
        ];
    }

    public function multiplesOfBothProvider(): array
    {
        return [
            [
                [
                    15 => 'StarCorpianos',
                    30 => 'StarCorpianos',
                    45 => 'StarCorpianos',
                    60 => 'StarCorpianos',
                    75 => 'StarCorpianos',
                    90 => 'StarCorpianos',
                ]
            ]
        ];
    }

    public function mergeMultiplesProvider(): array
    {
        return [
            [
                [
                    1 => 1,
                    2 => 2,
                    3 => 'StarCorp',
                    4 => 4,
                    5 => 'IT',
                    6 => 'StarCorp',
                    7 => 7,
                    8 => 8,
                    9 => 'StarCorp',
                    10 => 'IT',
                    11 => 11,
                    12 => 'StarCorp',
                    13 => 13,
                    14 => 14,
                    15 => 'StarCorpianos',
                    16 => 16,
                    17 => 17,
                    18 => 'StarCorp',
                    19 => 19,
                    20 => 'IT',
                    21 => 'StarCorp',
                    22 => 22,
                    23 => 23,
                    24 => 'StarCorp',
                    25 => 'IT',
                    26 => 26,
                    27 => 'StarCorp',
                    28 => 28,
                    29 => 29,
                    30 => 'StarCorpianos',
                    31 => 31,
                    32 => 32,
                    33 => 'StarCorp',
                    34 => 34,
                    35 => 'IT',
                    36 => 'StarCorp',
                    37 => 37,
                    38 => 38,
                    39 => 'StarCorp',
                    40 => 'IT',
                    41 => 41,
                    42 => 'StarCorp',
                    43 => 43,
                    44 => 44,
                    45 => 'StarCorpianos',
                    46 => 46,
                    47 => 47,
                    48 => 'StarCorp',
                    49 => 49,
                    50 => 'IT',
                    51 => 'StarCorp',
                    52 => 52,
                    53 => 53,
                    54 => 'StarCorp',
                    55 => 'IT',
                    56 => 56,
                    57 => 'StarCorp',
                    58 => 58,
                    59 => 59,
                    60 => 'StarCorpianos',
                    61 => 61,
                    62 => 62,
                    63 => 'StarCorp',
                    64 => 64,
                    65 => 'IT',
                    66 => 'StarCorp',
                    67 => 67,
                    68 => 68,
                    69 => 'StarCorp',
                    70 => 'IT',
                    71 => 71,
                    72 => 'StarCorp',
                    73 => 73,
                    74 => 74,
                    75 => 'StarCorpianos',
                    76 => 76,
                    77 => 77,
                    78 => 'StarCorp',
                    79 => 79,
                    80 => 'IT',
                    81 => 'StarCorp',
                    82 => 82,
                    83 => 83,
                    84 => 'StarCorp',
                    85 => 'IT',
                    86 => 86,
                    87 => 'StarCorp',
                    88 => 88,
                    89 => 89,
                    90 => 'StarCorpianos',
                    91 => 91,
                    92 => 92,
                    93 => 'StarCorp',
                    94 => 94,
                    95 => 'IT',
                    96 => 'StarCorp',
                    97 => 97,
                    98 => 98,
                    99 => 'StarCorp',
                    100 => 'IT',
                ]
            ]
        ];
    }
}