<?php

include __DIR__ . '/vendor/autoload.php';

use App\StarCorp;

$starCorp = new StarCorp();
$starCorp->run();
