<?php
namespace App\Contracts;

Interface MultipleInterface
{
    public function convertMultiples(array $multiple, array $factor, string $word): array;
}
