<?php

namespace Test\Unit;

use App\MultipleOfThree;
use PHPUnit\Framework\TestCase;

class MultipleOfThreeTest extends TestCase
{
    public function testWordWasDefinedToMultiplesOfThree()
    {
        $this->assertSame('StarCorp', MultipleOfThree::WORD);
    }

    /**
     * @dataProvider Test\Providers\MultiplesProvider::multiplesOfThreeProvider
     */
    public function testConvertMultiplesReturnsRuleForThree($data): void
    {
        $instance = new MultipleOfThree();
        $this->assertEquals(
            $data,
            $instance->convertMultiples(range(1, 100), [3, 3], MultipleOfThree::WORD)
        );
    }
}
